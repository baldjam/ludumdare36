﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class MainMenu : MonoBehaviour
    {
        public void Click_LoadLevel(string level)
        {
            SceneManager.LoadScene(level);
        }

        public void Click_RestartLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public void Click_Quit()
        {
            Application.Quit();
        }
    }
}
