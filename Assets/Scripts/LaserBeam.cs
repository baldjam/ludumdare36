﻿using UnityEngine;

namespace Assets.Scripts
{
    public class LaserBeam : MonoBehaviour
    {
        private Vector3 _target;
        private float _time;

        private float _maxTime = 4f;

        // Update is called once per frame
        // ReSharper disable once UnusedMember.Local
        void Update()
        {
            if (_target.Equals(Vector3.zero))
            {
                return;
            }

            transform.position = Vector2.MoveTowards(transform.position, _target, 0.2f);
            _time += Time.deltaTime;

            if (_time > _maxTime)
            {
                Destroy(gameObject);
            }

            if (Mathf.Approximately(transform.position.magnitude, _target.magnitude))
            {
                Destroy(gameObject);
            }
        }

        public void SetTarget(Vector3 targetPosition)
        {
            _target = targetPosition;

            float targetX = _target.x - transform.position.x;
            float targetY = _target.y - transform.position.y;
            float angle = Mathf.Atan2(targetY, targetX) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.tag == "Player")
            {
                other.gameObject.GetComponent<JeffHealth>().TakeDamage(gameObject);
            }

            DestroyObject(gameObject);
        }
    }
}
