﻿using Assets.Scripts.Weaponry;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Inventory : MonoBehaviour
    {
        public AudioClip PickupSfx;

        public SturdyPhone CurrentWeapon { get; private set; }
        public bool CanFire { get; set; }
        public bool CanBlock { get; set; }

        public void AddWeapon(SturdyPhone weapon)
        {
            CurrentWeapon = weapon;
            CanFire = true;
        }

        private void AddShield()
        {
            CanBlock = true;
        }

        // ReSharper disable once UnusedMember.Local
        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == "WeaponPickup")
            {
                switch (other.GetComponent<WeaponPickup>().Weapon)
                {
                    case Weapon.SturdyPhone:
                        AddWeapon(other.GetComponent<WeaponPickup>().Phone);
                        break;
                }

                SoundHelper.PlaySound(PickupSfx, transform.position);

                Destroy(other.gameObject);
            }
            else if (other.tag == "ShieldPickup")
            {
                AddShield();
                SoundHelper.PlaySound(PickupSfx, transform.position);

                Destroy(other.gameObject);
            }
        }
    }
}
