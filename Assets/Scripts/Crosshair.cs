﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Crosshair : MonoBehaviour
    {
        // Update is called once per frame
        // ReSharper disable once UnusedMember.Local
        void Update()
        {
            Vector3 newPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);
            transform.position = newPosition;
        }
    }
}
