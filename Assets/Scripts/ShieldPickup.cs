﻿using Assets.Scripts.Weaponry;
using UnityEngine;

namespace Assets.Scripts
{
    public class ShieldPickup : MonoBehaviour
    {
        public Weapon Weapon;
        public CRTShield Shield;
    }
}
