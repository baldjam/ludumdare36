﻿using Assets.Scripts.Weaponry;
using UnityEngine;

namespace Assets.Scripts
{
    public class JeffAttack : MonoBehaviour
    {
        public AudioClip AttackSfx;
        public CRTShield Shield;
        private Inventory _inventory;
        private GameObject _crosshair;

        // Use this for initialization
        // ReSharper disable once UnusedMember.Local
        void Start()
        {
            _inventory = GetComponent<Inventory>();
            _crosshair = GameObject.Find("Crosshair");
            Physics2D.IgnoreLayerCollision(8, 9);
        }

        // Update is called once per frame
        // ReSharper disable once UnusedMember.Local
        void Update()
        {
            if (!Shield.ShieldRaised && Input.GetAxisRaw("Fire1") > 0.1f && _inventory.CurrentWeapon != null && _inventory.CanFire)
            {
                _inventory.CanFire = false;

                Vector3 target = Camera.main.ScreenToWorldPoint(_crosshair.transform.position);
                target.z = 0;

                GameObject weapon = (GameObject)Instantiate(_inventory.CurrentWeapon.gameObject, transform.position, Quaternion.identity);

                Vector3 direction = target - transform.position;
                
                weapon.GetComponent<Rigidbody2D>().AddForce(direction * 5, ForceMode2D.Impulse);
                SoundHelper.PlaySound(AttackSfx, transform.position);
            }
        }
    }
}
