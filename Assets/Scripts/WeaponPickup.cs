﻿using Assets.Scripts.Weaponry;
using UnityEngine;

namespace Assets.Scripts
{
    public class WeaponPickup : MonoBehaviour
    {
        public Weapon Weapon;
        public SturdyPhone Phone;
    }
}
