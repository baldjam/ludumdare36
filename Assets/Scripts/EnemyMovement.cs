﻿using UnityEngine;

namespace Assets.Scripts
{
    public class EnemyMovement : MonoBehaviour
    {
        public float SightMaxDistance;

        private GameObject _jeff;
        private readonly Vector3 _speed = new Vector2(0.05f, 0);

        void Start()
        {
            _jeff = GameObject.FindWithTag("Player");
        }

        // Update is called once per frame
        // ReSharper disable once UnusedMember.Local
        void Update()
        {
            Move();
            StandUpStraight();
        }

        private void Move()
        {
            if (Vector3.Distance(transform.position, _jeff.transform.position) <= SightMaxDistance)
            {
                if (JeffIsRightOfEnemy())
                {
                    transform.position += _speed;
                }
                else // Jeff is Left of Enemy
                {
                    transform.position -= _speed;
                }
            }
        }

        private void StandUpStraight()
        {
            if (transform.rotation.z > 0.6f || transform.rotation.z < -0.6f)
            {
                float rotationStep = 600f * Time.deltaTime;

                RotateTowardsUp(rotationStep);
            }
            else if (!Mathf.Approximately(transform.rotation.z, 0f))
            {
                float rotationStep = 300f * Time.deltaTime;

                RotateTowardsUp(rotationStep);
            }
        }


        private void RotateTowardsUp(float rotationStep)
        {
            Quaternion newRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0f);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, newRotation, rotationStep);
        }
        private bool JeffIsRightOfEnemy()
        {
            return _jeff.transform.position.x > transform.position.x;
        }
    }
}
