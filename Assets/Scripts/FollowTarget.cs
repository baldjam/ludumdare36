﻿using UnityEngine;

namespace Assets.Scripts
{
    public class FollowTarget : MonoBehaviour
    {
        public Vector3 Offset;
        public Transform Target;
        public int CameraWindowWidth;

        void Start()
        {
            if (Target == null)
            {
                Target = GameObject.FindGameObjectWithTag("Player").transform;
            }
        }

        // Update is called once per frame
        // ReSharper disable once UnusedMember.Local
        void Update()
        {
            Vector3 newPosition = new Vector3(transform.position.x, Target.position.y, 0f);
            newPosition += Offset;

            if ((Target.position.x - transform.position.x) > CameraWindowWidth)
            {
                newPosition.x = Target.position.x - CameraWindowWidth;
            }
            else if ((Target.position.x - transform.position.x) < (-CameraWindowWidth))
            {
                newPosition.x = Target.position.x + CameraWindowWidth;
            }

            transform.position = newPosition;
        }
    }
}
