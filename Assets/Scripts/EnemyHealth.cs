﻿using UnityEngine;

namespace Assets.Scripts
{
    public class EnemyHealth : MonoBehaviour
    {
        public int Health;
        public AudioClip HurtSfx;
        public AudioClip DeathSfx;
        
        public void TakeDamage()
        {
            Health -= 1;

            if (Health <= 0)
            {
                KillEnemy();
            }
            else
            {
                SoundHelper.PlaySound(HurtSfx, transform.position);
            }
        }

        private void KillEnemy()
        {
            Vector3 playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
            SoundHelper.PlaySound(DeathSfx, playerPosition);
            Destroy(gameObject);
        }
    }
}
