﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class EndingFlag : MonoBehaviour
    {
        public float TimeToNextLevel;
        public AudioClip EndLevelSfx;
        
        private Animator _animator;
        private bool _levelEnded;
     
        // Use this for initialization
        void Start()
        {
            _animator = GetComponent<Animator>();
        }

        void Update()
        {
            if (!_levelEnded)
            {
                return;
            }

            TimeToNextLevel -= Time.deltaTime;

            if (TimeToNextLevel < 0)
            {
                int currentLevel = SceneManager.GetActiveScene().buildIndex;
                SceneManager.LoadScene(currentLevel + 1);
            }
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (_levelEnded)
            {
                return;
            }

            if (other.tag == "Player")
            {
                EndLevel();
            }
        }

        private void EndLevel()
        {
            _levelEnded = true;
            SoundHelper.PlaySound(EndLevelSfx, transform.position);
            _animator.SetTrigger("Raise flag");
        }
    }
}
