using UnityEngine;

namespace Assets.Scripts.Weaponry
{
    public class CRTShield : MonoBehaviour
    {
        public JeffHealth JeffHealth;

        private SpriteRenderer _sprite;
        private Collider2D _collider;
        private AudioSource _audioSource;

        public bool ShieldRaised { get; private set; }

        void Start()
        {
            _sprite = GetComponent<SpriteRenderer>();
            _collider = GetComponent<Collider2D>();
            _audioSource = GetComponent<AudioSource>();
        }

        public void RaiseShield()
        {
            _sprite.enabled = true;
            _collider.enabled = true;
            ShieldRaised = true;
            _audioSource.Play();
        }

        public void LowerShield()
        {
            _sprite.enabled = false;
            _collider.enabled = false;
            ShieldRaised = false;
            _audioSource.Stop();
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            if (ShieldRaised && other.gameObject.tag == "Laser")
            {
                JeffHealth.BlockLaser(other.gameObject);
                Destroy(other.gameObject);
            }
        }
    }
}