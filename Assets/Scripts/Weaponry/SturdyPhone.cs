﻿using System.Threading;
using UnityEngine;

namespace Assets.Scripts.Weaponry
{
    public class SturdyPhone : MonoBehaviour
    {
        public string Name
        {
            get { return "Sturdy Phone"; }
        }

        private float _liveTime;
        private Inventory _inventory;

        // ReSharper disable once UnusedMember.Local
        void Start()
        {
            _liveTime = 0;
            _inventory = GameObject.FindWithTag("Player").GetComponent<Inventory>();
        }

        void Update()
        {
            _liveTime += Time.deltaTime;

            if (_liveTime > 2f)
            {
                DestroySelf();
            }
        }

        private void DestroySelf()
        {
            _inventory.CanFire = true;
            Destroy(gameObject);
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.tag == "Enemy")
            {
                var enemy = other.gameObject.GetComponent<EnemyHealth>();
                enemy.TakeDamage();
                DestroySelf();
            }
            else if (other.gameObject.tag == "Destroyable")
            {
                Destroy(other.gameObject);
                DestroySelf();
            }
        }
    }
}
