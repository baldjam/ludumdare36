﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts
{
    public class JeffMove : MonoBehaviour
    {
        public AudioClip JumpSfx;

        private readonly Vector2 _walkingSpeed = new Vector2(0.1f, 0);
        private Animator _animator;
        private Rigidbody2D _rigidbody;
        private bool _jumping;
        private bool _canRotate;

        // Use this for initialization
        // ReSharper disable once UnusedMember.Local
        void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
            _animator = GetComponent<Animator>();
            Physics2D.IgnoreLayerCollision(8, 12);
        }

        // Update is called once per frame
        // ReSharper disable once UnusedMember.Local
        void Update()
        {
            CheckIfGrounded();
            Walk();
            Jump();
            StandUpStraight();
        }

        private void StandUpStraight()
        {
            if (!_canRotate)
            {
                return;
            }

            if (transform.rotation.z > 0.6f || transform.rotation.z < -0.6f)
            {
                float rotationStep = 600f * Time.deltaTime;

                RotateTowardsUp(rotationStep);
            }
            else if (!Mathf.Approximately(transform.rotation.z, 0f))
            {
                float rotationStep = 300f * Time.deltaTime;

                RotateTowardsUp(rotationStep);
            }
        }

        private void RotateTowardsUp(float rotationStep)
        {
            Quaternion newRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, 0f);

            transform.rotation = Quaternion.RotateTowards(transform.rotation, newRotation, rotationStep);
        }

        private void Jump()
        {
            if (!_jumping && JumpCommandReceived())
            {
                _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, 0f);
                _rigidbody.AddForce(Vector2.up * 7f, ForceMode2D.Impulse);

                SoundHelper.PlaySound(JumpSfx, transform.position);

                _jumping = true;
                _canRotate = false;
            }
        }

        private static bool JumpCommandReceived()
        {
            return Input.GetAxisRaw("Vertical") > 0.1f || Input.GetAxisRaw("Jump") > 0.1f;
        }

        private void CheckIfGrounded()
        {
            if (_jumping && Physics2D.Raycast(transform.position, Vector2.down, 0.52f, LayerMaskForJumping()))
            {
                _jumping = false;
                _canRotate = true;
            }
        }

        private int LayerMaskForJumping()
        {
            int layerMask = (1 << 8) + (1 << 9) + (1 << 10) + (1 << 11) + (1 << 12);
            return ~layerMask;
        }

        private void Walk()
        {
            Vector3 movementSpeed;

            if (Input.GetAxisRaw("Horizontal") > 0.1f)
            {
                transform.localScale = new Vector2(1, transform.localScale.y);
                movementSpeed = _walkingSpeed;
                _animator.SetBool("Walking", true);
            }
            else if (Input.GetAxisRaw("Horizontal") < -0.1f)
            {
                transform.localScale = new Vector2(-1, transform.localScale.y);
                movementSpeed = _walkingSpeed * -1;
                _animator.SetBool("Walking", true);
            }
            else
            {
                movementSpeed = Vector2.zero;
               _animator.SetBool("Walking", false);
            }

            if ((movementSpeed.x > 0 && _rigidbody.velocity.x < 0) ||
                (movementSpeed.x < 0 && _rigidbody.velocity.x > 0))
            {
                movementSpeed = CalculateVelocityChange(movementSpeed);
            }

            transform.position += movementSpeed;
        }

        private Vector3 CalculateVelocityChange(Vector3 rightMotion)
        {
            float difference = _rigidbody.velocity.x;
            _rigidbody.velocity = new Vector3(_rigidbody.velocity.x + rightMotion.x, _rigidbody.velocity.y);

            rightMotion = Math.Abs(rightMotion.x) > Math.Abs(difference)
                ? new Vector3(rightMotion.x + difference, 0f)
                : Vector3.zero;
            return rightMotion;
        }
    }
}
