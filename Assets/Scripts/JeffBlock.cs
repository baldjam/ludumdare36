﻿using Assets.Scripts;
using Assets.Scripts.Weaponry;
using UnityEngine;

namespace Assets
{
    public class JeffBlock : MonoBehaviour
    {
        public CRTShield Shield;

        private Inventory _inventory;

        void Start()
        {
            _inventory = GetComponent<Inventory>();
            Physics2D.IgnoreLayerCollision(9, 12);
        }

        void Update()
        {
            if (!_inventory.CanBlock)
            {
                return;
            }

            if (Input.GetAxisRaw("Fire2") > 0.1f && !Shield.ShieldRaised)
            {
                Shield.RaiseShield();
            }
            else if (Input.GetAxisRaw("Fire2") < 0.1f && Shield.ShieldRaised)
            {
                Shield.LowerShield();
            }
        }
    }
}
