﻿using UnityEngine;

namespace Assets.Scripts
{
    public class EnemyAttack : MonoBehaviour
    {
        public GameObject LaserBeam;
        public int LaserRange;
        public float RechargeTime;
        public AudioClip LaserSfx;

        private GameObject _jeff;
        private float _laserRecharge;

        // Use this for initialization
        void Start()
        {
            _jeff = GameObject.FindWithTag("Player");
            Physics2D.IgnoreLayerCollision(10, 11);
        }

        // Update is called once per frame
        void Update()
        {
            if (_laserRecharge > 0)
            {
                _laserRecharge -= Time.deltaTime;
                return;
            }

            if (Vector3.Distance(transform.position, _jeff.transform.position) <= LaserRange)
            {
                Quaternion laserRotation = Quaternion.identity;

                GameObject laser = (GameObject)Instantiate(LaserBeam, transform.position, laserRotation);

                laser.GetComponent<LaserBeam>().SetTarget(_jeff.transform.position);
                SoundHelper.PlaySound(LaserSfx, transform.position);
                _laserRecharge = RechargeTime;
            }
        }
    }
}
