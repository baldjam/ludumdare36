using UnityEngine;

namespace Assets.Scripts
{
    public static class SoundHelper
    {
        public static void PlaySound(AudioClip soundEffect, Vector3 position)
        {
            if (soundEffect == null)
            {
                return;
            }

            AudioSource.PlayClipAtPoint(soundEffect, position);
        }

        public static void PlaySound(AudioClip soundEffect, Vector3 position, float volume)
        {
            if (soundEffect == null)
            {
                return;
            }

            AudioSource.PlayClipAtPoint(soundEffect, position, volume);
        }
    }
}