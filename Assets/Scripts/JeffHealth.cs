﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class JeffHealth : MonoBehaviour
    {
        public int Health;
        public Text GameOver;
        public GameObject RestartButton;
        public AudioClip HurtSfx;
        public AudioClip DeadSfx;

        private List<GameObject> _lasers = new List<GameObject>();
        private GameObject _crosshair;

        void Start()
        {
            GameOver = GameObject.Find("JeffFailedText").GetComponent<Text>();
            _crosshair = GameObject.Find("Crosshair");
        }

        public void TakeDamage(GameObject laser)
        {
            if (Health < 1)
            {
                return;
            }

            if (_lasers.Contains(laser))
            {
                return;
            }

            Health -= 1;

            if (Health < 1)
            {
                KillJeff();
            }
            else
            {
                SoundHelper.PlaySound(HurtSfx, transform.position);
            }
        }

        private void KillJeff()
        {
            SoundHelper.PlaySound(DeadSfx, transform.position);
            gameObject.GetComponent<JeffMove>().enabled = false;
            gameObject.GetComponent<JeffAttack>().enabled = false;
            gameObject.GetComponent<JeffBlock>().enabled = false;
            GameOver.enabled = true;
            Destroy(_crosshair);
            RestartButton.SetActive(true);
        }

        public void BlockLaser(GameObject blockedLaser)
        {
            _lasers.Add(blockedLaser);
        }

        public void RemoveLaser(GameObject laser)
        {
            _lasers.Remove(laser);
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (Health < 1)
            {
                return;
            }

            if (other.tag == "Killbox")
            {

                KillJeff();
            }
        }
    }
}
